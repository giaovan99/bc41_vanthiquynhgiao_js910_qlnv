function localStorageDuLieu(){
    // convert array DSSV thành JSON\
    var dsnvJson = JSON.stringify(DSNV);
    console.log(DSNV);
    console.log(dsnvJson);
    // lưu Json vào LocalStorage
    localStorage.setItem('DSNV_LOCAL',dsnvJson);
}

function layThongTinTuForm(){
    // lấy thông tin từ form
    var _maNV = document.getElementById('tknv').value;
    var _tenNV = document.getElementById('name').value;
    var _email = document.getElementById('email').value;
    var _pass = document.getElementById('password').value;
    var _ngayLam = document.getElementById('datepicker').value;
    var _luongCB = document.getElementById('luongCB').value*1;
    var _role = document.getElementById('chucvu').value;
    var _gioLam = document.getElementById('gioLam').value*1;
    
    // tạo đối tượng nhân viên
    var nv = new NhanVien(_maNV,
        _tenNV,
        _email,
        _pass,
        _ngayLam,
        _luongCB,
        _role,
        _gioLam);
    console.log(nv);
    // trả về nv
    return nv;
};

function render(){
    let parseList = DSNV.map((item)=>{
        var nv = new NhanVien(
            item.maNV,
            item.tenNV,
            item.email,            
            item.pass,
            item.ngayLam,
            item.luongCB,
            item.role,
            item.gioLam);
            return nv;})
    
    var trHTML='';
    for (var index=0; index<parseList.length; index++){
        trHTML=trHTML+`<tr>
                            <td>${parseList[index].maNV}</td>
                            <td>${parseList[index].tenNV}</td>
                            <td>${parseList[index].email}</td>
                            <td>${parseList[index].ngayLam}</td>
                            <td>${parseList[index].role}</td>                          
                            <td>${parseList[index].tongLuong()}</td>
                            <td>${parseList[index].xepLoai()}</td>
                            <td>
                                <button id="${parseList[index].maNV}" 
                                        class="btn btn-danger" 
                                        onclick="xoaNV('${parseList[index].maNV}')"
                                        >Xoá</button>
                                <button id="${parseList[index].maNV}" 
                                        class="btn btn-warning"
                                        onclick="suaNV('${parseList[index].maNV}')"
                                        data-toggle="modal"
                                        data-target="#myModal"
                                        >Sửa</button>
                            </td>
                        </tr>`
    };
    document.getElementById('tableDanhSach').innerHTML=trHTML;
};

function timKiemViTri(id){
    var viTri=-1;
    for(var index=0; index<DSNV.length; index++){
        if(id==DSNV[index].maNV){
            viTri=index;
            break;
        }
    };
    return viTri;
}